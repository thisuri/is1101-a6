#include <stdio.h>
/*This program prints a number triangle*/

//prints numbers pattern
void pattern(int n);

//prints n th line of the pattern
void line(int n);


void line(int n){
	if(n==1)
		printf("1");
	else{
		printf("%d ",n);
		line(n-1);
	}	
}

void pattern(int n){
	int x;
	if(n==1)
		printf("1\n");
	else{ 
		pattern(n-1);
		line(n);
		printf("\n");
	}
}

void main(){
	int n;
	printf("Enter a number ");
	scanf("%d",&n);
	pattern(n);
}


