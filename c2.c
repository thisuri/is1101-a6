#include <stdio.h>
//This program prints fibonacci sequence up to n.

//prints fibonacci sequence up to n
void fibonacciSeq(int n);

void fibonacciSeq(int n){
	static int x[]={};
	if(n==0){
		x[n]=0;
		printf("%d\n",x[n]);
	}
	else if(n==1){
		fibonacciSeq(n-1);
		x[n]=1;
		printf("%d\n",x[n]);
	}
	else{
		fibonacciSeq(n-1);
		x[n]=x[n-1]+x[n-2];
		printf("%d\n",x[n]);
	}			
}

void main(){
	int x;
	printf("Enter the number of elements ");
	scanf("%d",&x);
	fibonacciSeq(x);
}
